export function setState(state) {
    return {
        type: 'SET_STATE',
        state
    };
}

export function plusState(val) {
    console.log(val)
    return {
        type: 'PLUS_STATE',
        state_map: val
    };
}


export function searchClasses(key,val) {
    return {
        type: 'SEARCH_CLASSES',
        data_search: {
          key: key,
          word: val
        }
    };
}

export function setValueTestItem(key,val) {
    return {
        type: 'SET_VALUE_TEST_ITEM',
        data_search: {
            key: key,
            word: val
        }
    };
}