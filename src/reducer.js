import {List,Map} from 'immutable';
import {SearchItem} from './requests/search';

function setState(state,newState){
    return state.set("values",newState);
}

function plusState(state,newState){
    const v = state.getIn(["values","progress_bar","width_progress"])
    const o = state.getIn(["values","progress_bar","offset_progress"])
    // return state.update(["values","width_progress"], v + newState)
    return state.set("values",Map({progress_bar: Map({width_progress: v + newState,offset_progress: o + newState})}))
}

function searchClasses(state,data_search){
    return state.setIn(
        ["values","test_items",data_search.key,"cont_for_search"],
        SearchItem(data_search.word))
}

function setValueTestItem(state,data_search){
    return state.setIn(["values","test_items",data_search.key,"value"],data_search.word)
}

function getState(state){
    return console.log(state.getIn(["values"]))
}

export default function(state = Map(), action) {
    switch (action.type) {
        case 'PLUS_STATE':
            return plusState(state,action.state_map)

        case 'SEARCH_CLASSES':
            return searchClasses(state,action.data_search)
        case 'SET_VALUE_TEST_ITEM':
            return setValueTestItem(state,action.data_search)

        case 'GET_STATE':
            return getState(state)
        case 'DECREMENT':
            return state.values - 1
        case 'SET_STATE':
            return setState(state,action.state_map)
        default:
            return state
    }
}