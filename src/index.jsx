import {scroll} from "./triggers/scroll";
import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Switch} from 'react-router';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {setState} from './action_creators';
import createHistory from 'history/createBrowserHistory'
// import ProgressBar from './components/ProgressBar';
import {Map,List} from 'immutable';
import reducer from "./reducer"
import {ProgressBarContainer} from './components/ProgressBar';
import {TestBodyContainer} from './components/test_page/TestBody';

const history = createHistory();

let store = createStore(reducer)

store.subscribe(() =>
    console.log(store.getState())
)


const progress_bar = Map({width_progress: 30,offset_progress: 25})

const test_items = Map(
        {
            "1": Map({
                key: "1",
                color: "#ffffff",
                value: "",
                cont_for_search: []
            })
            , "2": Map({
                key: "2",
                color: "#8B572A",
                value: "",
                cont_for_search: []
            })
            , "3": Map({
                key: "3",
                color: "#4990E2",
                value: "",
                cont_for_search: []
            })
        }
    )

const result_arr = Map({
    progress_bar: progress_bar,
    test_items: test_items
})


store.dispatch({ type: 'SET_STATE',state_map: Map(result_arr)})
//
// store.dispatch({ type: 'GET_STATE',state: 10})

// 10

// store.dispatch({ type: 'INCREMENT' })
//
// store.dispatch({ type: 'INCREMENT' })
//
// store.dispatch({ type: 'DECREMENT' })


ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <div>
                <ProgressBarContainer />
                <div className="container test">
                    <div className="test-title text-center">
                        Могло бы быть и лучше?
                    </div>
                    <Route path="/" component={TestBodyContainer} />
                    <div className="text-center">
                        <a className="test-body__next-step" onClick={() => store.dispatch({ type: 'PLUS_STATE',state_map: 5})} href="#">
                            Продолжить
                        </a>
                        {/*<a class="test-body__next-step" href="javascript:void(0)"></a>*/}
                    </div>


                    {/*</a>*/}
                </div>
            </div>
        </Router>
    </Provider>,
    document.getElementById('app')
);


scroll();