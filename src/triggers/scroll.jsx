import $ from "jquery";

export function scroll() {
  // var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
  // console.log($(".progress-bar").offset().top);
  // console.log($(".progress-bar").offset().top);
  // console.log(document.getElementsByClassName("progress-bar").offset);
    setTimeout(function () {
        var ksScrolly = new KS_Scrolly(".progress-bar",".progress-bar-shadow");
    }, 100)

}


class KS_Scrolly {

    constructor(el,el_shadow) {
        // this._scrollyElements = [];
        this.el = $(el);
        this.el_shadow = $(el_shadow);
        this.top_el = $(el).offset().top;
        this._init();
    }

    _init() {

        document.addEventListener('mousewheel', 	  (e) => this._scrollHandler(e), false);
        document.addEventListener('DOMMouseScroll', (e) => this._scrollHandler(e), false);
    }

    _scrollHandler(e) {
        // var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;

        var scrollTop = document.body.scrollTop;
        if (scrollTop > this.top_el ){
          this.el.addClass("fixed");
          this.el_shadow.show();
        }
        else{
          this.el.removeClass("fixed");
          this.el_shadow.hide();
        }

    }

}

// class KS_Scrolly {
//
//     constructor() {
//         this._scrollyElements = [];
//         this._init();
//     }
//
//     _init() {
//         // Gather all ks-scrolly-trigger elements and push them into array
//         this._gatherScrollyElements();
//
//         // Add event listeners for page scrolling
//         document.addEventListener('mousewheel', 	  (e) => this._scrollHandler(e), false);
//         document.addEventListener('DOMMouseScroll', (e) => this._scrollHandler(e), false);
//     }
//
//     _gatherScrollyElements() {
//
//         // Query select all ks-scrolly-trigger elements
//         var scrollyElements = document.querySelectorAll('[ks-scrolly-trigger]');
// //       console.log('scrollyElements: ', scrollyElements);
//
//         // Add found elements into the scrolly elements array
//         for(var i = 0; i < scrollyElements.length; i++) {
//
//             var scrollyElement    = scrollyElements[i],
//                 scrollyTriggerPt  = scrollyElement.getAttribute('ks-scrolly-trigger'),
//                 scrollyReversible = scrollyElement.getAttribute('ks-scrolly-reversible');
//
//             // Add ks-scrolly instance class
//             scrollyElement.classList.add('ks-scrolly-instance-' + i);
//
//             // Is this element reversible when scrolling back up?
//             scrollyReversible = (scrollyReversible !== null) ? true : false;
//
//             var scrollyTopOffset = Math.round(this._getEleOffset(scrollyElement).top);
//
//             // Determine actual pixel value for percentages
//             var lastChar = scrollyTriggerPt[scrollyTriggerPt.length-1];
//             if(lastChar === '%') {
//                 scrollyTriggerPt = scrollyTriggerPt.slice(0, -1); // Pop last char
//                 scrollyTriggerPt = Math.round(window.innerHeight * (scrollyTriggerPt / 100));
// // 				console.log('scrollyTriggerPt: ' + scrollyTriggerPt);
//             }
//
//             // If top offset less then viewport height,
//             // then set top offset to trigger point
//             if(scrollyTopOffset <= window.innerHeight) {
//                 scrollyTopOffset = scrollyTriggerPt;
//                 scrollyTriggerPt = 0;
//             }
//
//             // Set empty trigger point value to null
//             if(scrollyTriggerPt === '') scrollyTriggerPt = null;
//
//             // Push new scrolly element to the array
//             this._scrollyElements.push({
//                 selector:     scrollyElement,
//                 topOffset:    scrollyTopOffset,
//                 triggerPoint: scrollyTriggerPt,
//                 isReversible: scrollyReversible
//             });
//         }
// //       console.log('this._scrollyElements: ', this._scrollyElements);
//     }
//
//     _scrollHandler(e) {
//
//         var direction = (e.detail < 0 || e.wheelDelta > 0) ? 'UP' : 'DOWN',
//             scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
//
//         // Check if current scrollTop went beyond any of the trigger elements
//         for(var i = 0; i < this._scrollyElements.length; i++) {
//
//             var scrollyElement = this._scrollyElements[i];
//
//             // Determine trigger point
//             var elePos;
//             if(scrollyElement.triggerPoint === null) {
//                 // If trigger point not set, then use the element's top offset
//                 elePos = scrollyElement.topOffset;
//             }
//             else {
//                 elePos = scrollyElement.topOffset - scrollyElement.triggerPoint;
//             }
//
//             // Scrolling DOWN checking
//             if(scrollTop >= elePos && direction === 'DOWN') {
//                 scrollyElement.selector.classList.add('ks-triggered');
//             }
//             // Scrolling UP checking (only applies for reversible scrolly elements)
//             else if(scrollyElement.isReversible && direction === 'UP') {
//                 var eleHeight = this._getEleOuterHeight(scrollyElement.selector);
//                 if(scrollTop <= elePos + eleHeight) {
//                     scrollyElement.selector.classList.remove('ks-triggered');
//                 }
//             }
//         }
//     }
//
//     _getEleOffset(ele) {
//         var rect = ele.getBoundingClientRect(), bodyElt = document.body;
//
//         return {
//             top: rect.top + bodyElt .scrollTop,
//             left: rect.left + bodyElt .scrollLeft
//         }
//     }
//
//     _getEleOuterHeight(elm) {
//         var elmHeight, elmMargin;
//         if(document.all) { // IE
//             elmHeight = elm.currentStyle.height;
//             elmMargin = parseInt(elm.currentStyle.marginTop, 10) + parseInt(elm.currentStyle.marginBottom, 10);
//         } else { // Mozilla
//             elmHeight = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('height'));
//             elmMargin = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-top')) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-bottom'));
//         }
//         return (elmHeight+elmMargin);
//     }
// }
//
// document.addEventListener('DOMContentLoaded', function () {
//
//     // Make the hero logo appear
//     var $heroLogo = document.getElementById('hero-logo');
//     setTimeout(function () {
//         $heroLogo.classList.add('appear');
//     }, 1000);
//
//     // Init KS_Scrolly
//     var ksScrolly = new KS_Scrolly();
// });
//
