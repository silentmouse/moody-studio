import React from 'react';

class ContentSearch extends React.Component{
    render() {

        console.log(this.props.cont_for_search)

        return <div className="test-body-search-content">
            {this.props.cont_for_search.map(f =>
                <div key={f} className="test-body-search-content_item">{f}</div>
            )}
        </div>
    }
}

export const ContentForSearch = ContentSearch;