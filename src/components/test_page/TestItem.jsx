import React from 'react';
import {ContentForSearch} from "./ContentForSearch";

class TestItem extends React.Component{
    render() {

        return <div className="test-body-item">
            <div className="test-body-item__marker" style={{background: this.props.color}}>
            </div>
            <input type="text"
                   className="test-body-item__input"
                   placeholder="Алкоголь"
                   onChange={(f)=> {
                       this.props.searchClasses(this.props.h_key, f.target.value);
                       this.props.setValueTestItem(this.props.h_key, f.target.value)
                   }
                   }/>
            <ContentForSearch cont_for_search={this.props.cont_for_search}/>
        </div>
    }
}

export const TestItemCont = TestItem;