import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import * as actionCreators from '../../action_creators';
import {TestItemCont} from './TestItem';

class TestBody extends React.Component{
    render() {

        const {searchClasses,setValueTestItem} = this.props.actions

        return <div className="test-body">
            <div className="test-body__title">
                Выбери 3 любимых занятия:
            </div>
            {this.props.test_items.map(f =>
                <TestItemCont
                    key={f.get("key")}
                    h_key={f.get("key")}
                    color={f.get("color")}
                    cont_for_search={f.get("cont_for_search")}
                    searchClasses={searchClasses}
                    setValueTestItem = {setValueTestItem}
                />
            )
            }
        </div>
    }
}

const mapStateToProps = (state) => (
    {
       test_items: state.getIn(["values","test_items"]),
    }
)

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actionCreators, dispatch)
    }
}


export const TestBodyContainer = connect(mapStateToProps,mapDispatchToProps)(TestBody)