import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import * as actionCreators from '../action_creators';

class ProgressBar extends React.Component{
    // constructor(props) {
    //     super(props);
    //     this.state = props;
    //     this.handleClick = this.handleClick.bind(this)
    // }
    // handleClick() {
    //     this.props.actions.plusState(5)
    //
    // }
    render() {

        return <div>

            <div className="progress-bar">
                <div className="scale" style={{width: this.props.width + "%"}}>
                </div>
                <div className="number" style={{marginLeft: this.props.offset + "%"}}>
                    {this.props.width}%
                </div>
            </div>
            <div className="progress-bar-shadow">
            </div>
        </div>;
    }
}
//
// ProgressBar.defaultProps = {
//     width: 30,
//     offset: 25
// }


// ProgressBar.propTypes = {
//     width: PropTypes.float.isRequired,
//     offset: PropTypes.float.isRequired
// }
//
// export default function ProgressBar(props){
//     return <ProgressBar {...props}/>;
// };

const mapStateToProps = (state) => (
    {
        width: state.getIn(["values","progress_bar","width_progress"]),
        offset: state.getIn(["values","progress_bar","offset_progress"])
    }
)

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actionCreators, dispatch)
    }
}

export const ProgressBarContainer = connect(mapStateToProps,mapDispatchToProps)(ProgressBar)

// ProgressBar.get_width = {
//     width: "30%"
// };
//
// ProgressBar.get_margin = {
//     marginLeft: "30%"
// };

// export default React.createClass({
//     getWidth: function(){
//       return {width: '30%'};
//     },
//     getMargin: function(){
//       return {marginLeft: '30%'};
//     },
//     render: function () {
//         return <div>
//             <div className="progress-bar">
//                 <div className="scale" style={this.getWidth()}>
//                 </div>
//                 <div className="number" style={this.getMargin()}>
//                     30%
//                 </div>
//             </div>
//             <div className="progress-bar-shadow">
//             </div>
//         </div>;
//
//     }
// });